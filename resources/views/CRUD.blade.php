@extends('adminlte.master')

@section('data')

<div class="ml-3 mt-3">
    <div class="card card-primary">
        <div class="card-header">
            <h3>Pertanyaan</h3>
        </div>
            <form role="form" action="/posts" method="POST">
                <div class="card-body">
                    <div class= "ml-1">
                        <div class="form-group">
                            <label for="exampleInputJudul1">Judul</label>
                            <input type="text" class="form-control" id="exampleInputJudul1" placeholder="Masukan Judul">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputIsi1">Isi</label>
                            <input type="text" class="form-control" id="exampleInputIsi1" placeholder="Isi">
                        </div>
                        
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="exampleKirim1">
                            <label class="form-check-label" for="exampleKirim1">Kirim</label>
                        </div>
                        </div>

                        <div class="card-footer">
                            <button type="submit" class="btn btn primary"> Kirim </button>
                        </div>
                </div>
            </form>
    </div>
</div>
@endsection