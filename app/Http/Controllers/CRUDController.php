<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CRUDController extends Controller
{
    public function create() {
        return view('CRUD');
    }

    public function post(Request $request) {
        DB::table('pertanyaan')->insert([
            "Judul" => $request["judul"],
            "Isi" => $request["Isi"]
        ]);

        return redirect('/posts');
    }
}
